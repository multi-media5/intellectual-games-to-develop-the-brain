var points = 0; // Keep track of the number of points

var backgroundMusic = new Audio("Background Music2.mp3"); // Create an audio element for background music

backgroundMusic.volume = 0.5;
backgroundMusic.loop = true; // Set the audio to loop
backgroundMusic.play(); // Start playing the background music
var correctSound = new Audio("sound correct.mp3"); // Create an audio element for correct answers
var incorrectSound = new Audio("sound incorrect.mp3"); // Create an audio element for incorrect answers

function checkAnswer() {
  // Get the numbers and the player's answer
  var number1 = document.getElementById("number1").textContent;
  var number2 = document.getElementById("number2").textContent;
  var answer = document.getElementById("answer").value;

  // Check if the answer is correct
  if (parseInt(answer) === parseInt(number1) + parseInt(number2)) {
    points++; // Increment the point counter
    document.getElementById("points").textContent = points; // Update the point display
    document.getElementById("result").textContent = "Correct!";
    correctSound.play(); // Play the correct sound

    // Check if the points have reached 5
    if (points === 5) {
      // Add a button for moving to the next page
      var button = document.createElement("button");
      button.textContent = "Next";
      button.style.position = "absolute";
      button.style.top = "90px";
      button.style.left = "1420px";
      button.classList.add("btn");
      button.onclick = function () {
        window.location.href = "pang_Math2.html"; // navigate to file.html
      };
      document.body.appendChild(button);
    }

    // Set the numbers to new random values
    document.getElementById("number1").textContent = Math.floor(Math.random() * 10);
    document.getElementById("number2").textContent = Math.floor(Math.random() * 10);
  } else {
    points--; // Decrement the point counter
    document.getElementById("points").textContent = points; // Update the point display
    document.getElementById("result").textContent = "Incorrect. Try again.";
    document.getElementById("answer").value = ""; // Clear the answer field
    incorrectSound.play(); // Play the incorrect sound
  }
}


// Set the numbers to random values when the page loads
window.onload = function () {
  document.getElementById("number1").textContent = Math.floor(Math.random() * 10);
  document.getElementById("number2").textContent = Math.floor(Math.random() * 10);
}